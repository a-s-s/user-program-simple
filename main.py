import sys

from prokernel import prokernel


def center_rocket_angle(current_angle):
    # logic to center rocket
    if current_angle < -1:
        return 1
    elif current_angle > 1:
        return -1
    return 0


def bit_of_throttle():
    return 0.2


def ia_compute_new_frame(world_state):
    control = prokernel.Control(
        bit_of_throttle(),
        center_rocket_angle(world_state.user_missile.angle)
    )

    return control


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("need ip at least")
        exit(-1)

    target = sys.argv[1]

    port = 8080
    if len(sys.argv) > 2:
        port = int( sys.argv[2] )

    prokernel.init(target, port)

    while True:
        world_state = prokernel.receive()

        next_command = ia_compute_new_frame(world_state)

        prokernel.send(next_command)
