# User Program Simple

Clone this repository where you want :

```
git clone https://gitlab.com/a-s-s/user-program-simple.git
```

Install the dependencies :

```
pip3 install -r requirements.txt
```

Run the program with :

```
python3 main.py <IP> [port]
```

For instance, if you want to run it locally with the default port :

```
python3 main.py 127.0.0.1
```
